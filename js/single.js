// This code sample uses the 'node-fetch' library:
// https://www.npmjs.com/package/node-fetch
// const fetch = require('node-fetch');
const load = document.addEventListener("DOMContentLoaded", () => {
    data();
})

const input = document.querySelector(".addTxt")
document.addEventListener("keyup", function (event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        addElement()
    }
});

const boardId = {
    "name": "Red",
    "id": "605c12ab9e65220633eb1fb1"
};
const listId = {
    "id": "605c12ab9e65220633eb1fb4",
    "name": "Done",
    "closed": false,
    "pos": 49152,
    "softLimit": null,
    "idBoard": "605c12ab9e65220633eb1fb1",
    "subscribed": false
}

let cardList = [];
let list = [];

//updates the cards in the list
function addElement() {
    const list = document.querySelector(".addTxt").value
    if (list) {
        cardList.push({
            name: list,
            id: '',
            complete: false
        })
        input.value = ""
        // console.log(todoList[0].text)
        fetch(`https://api.trello.com/1/cards?key=d71118bbc2a871363416ddebdde1f69b&token=f01c685ff3583f725a159937a5add47553bef672fc1df6e5f49dda5e1854340a&idList=605c12ab9e65220633eb1fb4&name=${list}`, {
            method: "POST"
        })
        display()
    }
}

//gives the data of the lists in the red board
async function board() {
    await fetch('https://api.trello.com/1/boards/605c12ab9e65220633eb1fb1/lists?key=d71118bbc2a871363416ddebdde1f69b&token=f01c685ff3583f725a159937a5add47553bef672fc1df6e5f49dda5e1854340a', {
        method: 'GET'
    })
        .then(response => response.json())
        .then(res => {
            for (let i = 0; i < res.length; i++) {
                console.log("ok")
                list.push({
                    id: res[i].id,
                    name: res[i].name
                })
            }


        })
    console.log("list are", list)
}
board()

//gives the cards in the list(Done)
function data() {
    fetch('https://api.trello.com/1/lists/605c12ab9e65220633eb1fb4/cards?key=d71118bbc2a871363416ddebdde1f69b&token=f01c685ff3583f725a159937a5add47553bef672fc1df6e5f49dda5e1854340a', {
        method: 'GET'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`

            );
            // let responseText = response.text()
            return response.text()
        })
        .then(response => JSON.parse(response))
        // .then(text => console.log(text))
        .then(responseText => {
            cardList = []
            for (let i = 0; i < responseText.length; i++) {
                cardList.push({
                    id: responseText[i].id,
                    name: responseText[i].name,
                    complete: false,

                })
            }
            console.log(responseText)
            console.log('list is :', cardList)
            display()
        })
        .catch(err => console.error(err));

}

//deletes the cards in the list
function deleteItem(index) {
    fetch(`https://api.trello.com/1/cards/${cardList[index].id}?key=d71118bbc2a871363416ddebdde1f69b&token=f01c685ff3583f725a159937a5add47553bef672fc1df6e5f49dda5e1854340a`, {
        method: 'DELETE'
    })
    cardList.splice(index, 1);
    display()
}


//displays the data in the board.html
function display() {
    document.querySelector(".list").innerHTML = ""
    template = ""
    for (let i = 0; i < cardList.length; i++)
        if (cardList[i].complete == false)
            // document.querySelector(".list").innerHTML +
            template += "<div class='data'>" + "<div>" +
                "<input  type='checkbox' onclick='toggleStatus(" + i + ")'>" + `<span> ${cardList[i].name} </span>` + "</div>" +
                "<button onclick='deleteItem(" + i + ")'><img src='./icons/x.svg' alt=''></button></div>";
        else
            // document.querySelector(".list").innerHTML +
            template += "<div class='data'>" + "<div>" +
                "<input  type='checkbox' onclick='toggleStatus(" + i + ")' checked>" + `<span> <s> ${cardList[i].name} </s> </span>` + "</div>" +
                "<button onclick='deleteItem(" + i + ")'><img src='./x.svg' alt=''></button></div>";
    document.querySelector(".list").innerHTML += `<div id='head'><span>${listId.name}</span><img src='./icons/more-horizontal.svg' alt=''></div>` + template

}

//toggles the check status
function toggleStatus(index) {
    console.log(index);
    cardList[index]['complete'] = !cardList[index]['complete']
    display()
}
